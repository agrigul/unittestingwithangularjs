/// <reference path="/Scripts/_references.js"/>
/// <binding BeforeBuild='server' />

var gulp = require("gulp");
var browserSync = require("browser-sync");
var KarmaServer = require("karma").Server;
var LiveServer = require("gulp-live-server");


gulp.task("server", function () { // runs a server.js file as simple server
    var live = new LiveServer("server.js");
    live.start();
});

/*
 Runs the application on localhost:8080
*/
gulp.task("serve", ["server"], function () { // task for develop env in browsr

    browserSync.init({
        notify: false, // notifications from browserSync
        port: 8080,
        server: {
            baseDir: ["app"],
            routes: {
                '/bower_components': "bower_components"
            }
        }
    });

    gulp.watch(["app/**/*.*"]).on("change", browserSync.reload);  // watches to automatically reload
});


gulp.task("test-in-browser", function () {   // tasks for running unit tests and watch in browaser

    browserSync.init({
        notify: false,
        port: 8081,
        server: {
            baseDir: ["tests", "app"],  // sets the path where mocha searchs for files
            routes: {
                '/bower_components': "bower_components"
            }
        }
    });

    gulp.watch(["app/**/*.*", "tests/**/*.*"]).on("change", browserSync.reload);

});


gulp.task("test-in-console", function (done) {  // task for running karma and phantomJS for unit testing in a background process
    
    new KarmaServer.start({
            configFile: __dirname + "/karma.conf.js",
            singleRun: true,                // should be true to call done() function
            reporters: ["mocha", "coverage"]
        }, function () {
        done();
    });
});


gulp.task("test-coverage",["test-browser"],  function () {   // tasks to display test coverage

    browserSync.init({
        notify: false,
        port: 7777,
        server: {
            baseDir: ["tests/coverage"]  // sets the path where mocha searchs for files
        }
    });

    gulp.watch(["app/**/*.*"]).on("change", browserSync.reload);

});