/// <reference path="../Scripts/_references.js"/>
/**
 * Created by agriguletskiy on 02.08.2015.
 */
(function (angular) {
    "use strict";

    angular.module("AddressBook", [])
        .service("contactService", contactService)
        .controller("contactController", contractController)
        .filter("proper", properFilter)
        .directive("avatar", avatarDirective)
        .controller("AddContactController", addContactController);

    function contactService($http) {
        this.contacts = [];
        var contactService = this;

        $http.get("http://localhost:9000/contacts")
            .then(function (res) {
                console.log('res from get', res);
                while (res.data[0]) {
                    contactService.contacts.push(res.data.shift());
                }
            });


        this.addContact = function (contact) {
            var contactCopy = angular.copy(contact);
            contactService.contacts.push(contactCopy);
        };

    }
    function addContactController($scope, contactService) {
        
        $scope.addContact = function() {
            contactService.addContact($scope.contact);
        };
    }

    function contractController(contactService, $scope) {
        var ctrl = this;

        $scope.contacts = contactService.contacts;

        /*
         * Searches for a contact by name
         */
        ctrl.findContactByName = function (name) {
            if (!contactService || !contactService.contacts) {
                return null;
            }
            var contacts = contactService.contacts;

            for (var i = 0; i < contacts.length; i++) {
                if (contacts[i] === name) {
                    return contacts[i];
                }
            }

            return null;
        };
    }

    function properFilter() {
        return function (name) {
            var type = typeof name;
            if (type !== 'number' && type !== 'string')
                throw new Error();

            return name.toString().split(" ").map(function (word) {   // 1. split array to words with no spaces
                return word[0].toUpperCase().concat(word.slice(1));  // 2. make first letter of each word to upper case and join with other words
            }).join(" ");                                           // sam fisher  -->  Sam Fisher
        }
    }

    function avatarDirective() {

        return {
            restrict: "AE", // restricts to attributes or elements (most common)
            scope: {
                name: "="  // Name property must be defined explicitly in a parent
            },
            template: "<span class='avatar'>{{name[0] | proper }}</span>"
        }

    }

}(angular));