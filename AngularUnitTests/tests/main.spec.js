/// <reference path="../Scripts/_references.js"/>


(function (window, chai) {
    var assert = chai.assert;
    var expect = chai.expect;


    window.describe("The address book app", function () {

        window.describe("the contacts service", function () {

            var contactService,
                $httpBackend;

            // On test init
            beforeEach(function () {
                module("AddressBook");
                inject(function ($injector) {
                    contactService = $injector.get("contactService");
                    $httpBackend = $injector.get("$httpBackend");
                });
            });

            it("should have a property contacts, an array ", function () {
                expect(contactService.contacts).to.be.an('array');                
            });

            it("should call backend ", function () {
                var mockedData = ['1', '2', '3'];

                $httpBackend.expectGET("http://localhost:9000/contacts")
                    .respond(200, mockedData);

                $httpBackend.flush(); // make a request
                expect(contactService.contacts).to.be.deep.equal(mockedData);
            });
        });

        window.describe("the contacts controller", function () {

            var contactService,
                $httpBackend,
                $scope,
                $controller;

            // On test init
            beforeEach(function () {
                module("AddressBook");
                inject(function ($injector, $rootScope) {
                    contactService = $injector.get("contactService");
                    $httpBackend = $injector.get("$httpBackend");
                    var mockedData = ['notArtem', 'Igor', 'Artem', 'Mike'];

                    $httpBackend.expectGET("http://localhost:9000/contacts")
                        .respond(200, mockedData);
                    $httpBackend.flush();

                    $scope = $rootScope.$new();
                    $controller = $injector.get("$controller");
                });
            });

            it("should store array of contacts in scope", function () {
                $controller("contactController", { $scope: $scope, contactService: contactService });
                assert.isArray($scope.contacts);
            });

            // non working tests. Can't understand how to send contacts
            it("should return contact by name", function () {
                var ctrl = $controller("contactController", { $scope: $scope, contactService: contactService });
                var res = ctrl.findContactByName("Artem"); 
                assert.isTrue("Artem" === res);
            });
        });

        window.describe("the propeer filter", function () {

            var proper;

            beforeEach(function () {
                module("AddressBook");
                inject(function ($injector) {
                    proper = $injector.get("$filter")("proper");
                });

            });

            it("should  proper case a string", function () {
                expect(proper("ned stark")).to.equal("Ned Stark");
                expect(proper("cersei lannister")).to.equal("Cersei Lannister");
            });


            it("should take a number and retur as a string", function () {
                expect(proper(42)).to.equal("42");

            });

            it("should throw an error on an wrong type", function () {
                assert.throws(function() {
                    proper(undefined);
                });
            });
        });


        window.describe("avatar", function () {

            beforeEach(function () {
                module("AddressBook");

            });

            it("should display the capitalized first letter of a name", function () {
                inject(function ($rootScope, $compile) {
                    $rootScope.contact = { name: "jon arryn" };
                    var element = $compile("<avatar name='contact.name'/> ")($rootScope);
                    $rootScope.$digest();
                    var dirText = element.text();
                    expect(dirText).to.equal("J");

                });

            });

        });
    });

})(window, chai);