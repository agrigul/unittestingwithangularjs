/// <reference path="/Scripts/_references.js"/>

// Karma configuration
// Generated on Mon Aug 03 2015 01:05:44 GMT+0400 (Russian Standard Time)

module.exports = function (config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: "",


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ["mocha"],


        // list of files / patterns to load in the browser
        files: [
            "bower_components/angular/angular.js",
            "bower_components/angular-mocks/angular-mocks.js",
            "bower_components/chai/chai.js",
            "app/**/*.js",
            "tests/*.js"
        ],


        // list of files to exclude
        exclude: [],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            "app/**/*.js": ["coverage"] // says to karma-coverage (or istanbul?) to scan all our code to get % of covered code by tests
        },

        coverageReporter: {
            includeAllSources: true,
            reporters: [{
                type: "html",           // creates html version of coverage report. To inspecet http://www.localhost:8080/coverage
                dir: "tests/coverage",
                subdir : "."            // disables the creation of subdirectory like PhantomJS 2.1....
            },
            {
                type: "text"            // a text report for console
            }
            ]
        },

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ["progress", "mocha"],


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ["PhantomJS"],

        plugins: [
            "karma-mocha",
            "karma-coverage",
            "karma-yandex-launcher",
            "karma-phantomjs-launcher",
            "karma-mocha-reporter"
        ],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false
    });
}
