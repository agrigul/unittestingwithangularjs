/// <reference path="/Scripts/_references.js"/>
/// <reference path="~/node_modules/express/lib/application.js"/>


/**
 * Created by agriguletskiy on 03.08.2015.
 */
var express = require("express");
var cors = require("cors");

var app = express();
app.use(cors());

var contacts = [
    {
        name: "Artem"
    },
    {
        name: "Robert"
    }
];
//--------------------------------
///  some chnages in file

app.get("/contacts", function (req, res) {

    res.status(200).json(contacts);
});


app.listen(9000);

